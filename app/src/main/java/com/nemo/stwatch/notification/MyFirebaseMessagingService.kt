package com.nemo.stwatch.notification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.nemo.stwatch.R
import com.nemo.stwatch.activities.MainActivity
import com.nemo.stwatch.constants.APIConstant
import com.nemo.stwatch.constants.AppConstant
import com.nemo.stwatch.db.entity.SmsObj

class MyFirebaseMessagingService : FirebaseMessagingService() {

    /**
     * Called when msg is received.
     *
     * @param remoteMessage Object representing the msg received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        // Check if msg contains a data payload.
        remoteMessage.data.isNotEmpty().let {
//            Log.d(TAG, "Message data payload: " + remoteMessage.data)
//            Log.d(TAG, "Message data payload: " + remoteMessage.data.get("mob").toString())
//            Log.d(TAG, "Message data payload: " + remoteMessage.data.get("hwID").toString())
//            Log.d(TAG, "Message data payload: " + remoteMessage.data.get("msg").toString())
//            Log.d(TAG, "Message data payload: " + remoteMessage.data.get("timestamp").toString())
//            Log.d(TAG, "Message data payload: " + remoteMessage.data.get("createdAt").toString())
//            Log.e(TAG, "remoteMessage.data.get(\"logout\") "+ remoteMessage.data.get("logout").toString())

            if(remoteMessage.data.get("hwID") != null){
                var smsObj = SmsObj(
                    remoteMessage.data.get("mob").toString(),
                    remoteMessage.data.get("hwID").toString(),
                    remoteMessage.data.get("msg").toString(),
                    remoteMessage.data.get("timestamp").toString(),
                    remoteMessage.data.get("createdAt").toString(),
                    false, false)

                val localBroadcastManager = LocalBroadcastManager.getInstance(this)
                // Everything is squared away, let's signal we can start handling messages.
                localBroadcastManager.sendBroadcast(Intent(APIConstant.RECEIVER_ACTION))
            }else if(remoteMessage.data.get("logout") != null && (remoteMessage.data.get("logout").toString() == "true")){
                val localBroadcastManager = LocalBroadcastManager.getInstance(this)
                // Everything is squared away, let's signal we can start handling messages.
                var logoutIntent = Intent(APIConstant.RECEIVER_LOGOUT)
                logoutIntent.putExtra(AppConstant.KEY_INTENT_MESSAGE, remoteMessage.notification?.body)
                localBroadcastManager.sendBroadcast(logoutIntent)

                showNotification(remoteMessage.notification?.title, remoteMessage.notification?.body)
            }

            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use WorkManager.
                scheduleJob()
            } else {
                // Handle msg within 10 seconds
                handleNow()
            }
        }

        // Check if msg contains a notification payload.
        remoteMessage.notification?.let {
//            Log.d(TAG, "Message Notification Body: ${it.body}")
//            Log.d(TAG, "Message Notification title: ${it.title}")
        }

        if (remoteMessage.notification != null) {
            //showNotification(remoteMessage.notification?.title, remoteMessage.notification?.body)
        }
        // Also if you intend on generating your own notifications as a result of a received FCM
        // msg, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    // [START on_new_token]
    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    override fun onNewToken(token: String) {
//        Log.d(TAG, "Refreshed token: $token")

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(token)
    }
    // [END on_new_token]

    /**
     * Schedule async work using WorkManager.
     */
    private fun scheduleJob() {
        // [START dispatch_job]
        val work = OneTimeWorkRequest.Builder(MyWorker::class.java).build()
        WorkManager.getInstance().beginWith(work).enqueue()
        // [END dispatch_job]
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private fun handleNow() {
        Log.d(TAG, "Short lived task is done.")
    }

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private fun sendRegistrationToServer(token: String?) {
        // TODO: Implement this method to send token to your app server.
        Log.d(TAG, "sendRegistrationTokenToServer($token)")
    }

    /**
     * Create and show a simple notification containing the received FCM msg.
     *
     * @param messageBody FCM msg body received.
     */
    private fun sendNotification(messageBody: String) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
            PendingIntent.FLAG_ONE_SHOT)

        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.ic_stat_ic_notification)
            .setContentTitle(getString(R.string.fcm_message))
            .setContentText(messageBody)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
    }

    private fun showNotification(title: String?, body: String?) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent,
            PendingIntent.FLAG_ONE_SHOT)

        val soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle(title)
            .setContentText(body)
            .setAutoCancel(true)
            .setSound(soundUri)
            .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(0, notificationBuilder.build())
    }

    companion object {
        private const val TAG = "MyFirebaseMsgService"
    }
}