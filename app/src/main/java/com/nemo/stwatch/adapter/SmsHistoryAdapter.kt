package layout


import android.graphics.Color
import android.util.Log
import android.widget.TextView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.nemo.stwatch.R
import com.nemo.stwatch.constants.ColorConstant
import com.nemo.stwatch.db.entity.SmsObj

 class SmsHistoryAdapter : androidx.recyclerview.widget.RecyclerView.Adapter<SmsHistoryAdapter.NoteHolder>() {
    private var smsMessages: List<SmsObj> = ArrayList()
lateinit var onItemClick: (Int) -> Unit

     var TAG = "SmsHistoryAdapter"

     lateinit var onStartAckList: (Int) -> Unit
     lateinit var onStartViewList: (Int) -> Unit

     fun getMessages(): List<SmsObj>{
         return smsMessages
     }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.history_item, parent, false)
        return NoteHolder(itemView)
    }

    override fun onBindViewHolder(holder: NoteHolder, position: Int) {
        val currentNote = smsMessages[position]
        holder.smsContent.text = currentNote.msg
        holder.timeStamp.text = currentNote.timestamp

        holder.imgAck.setOnClickListener {
            onStartAckList(position)
        }

        holder.imgProfile.setOnClickListener {
            onStartViewList(position)
        }

        if(position%2 == 0){
            holder.separator.setBackgroundColor((Color.parseColor(ColorConstant.SEPARATOR)))
            holder.card.setBackgroundColor(Color.WHITE)
        }else{
            holder.separator.setBackgroundColor(Color.WHITE)
            holder.card.setBackgroundColor(Color.parseColor(ColorConstant.SEPARATOR))
        }

        holder.card.setOnClickListener{
        }
    }

    override fun getItemCount(): Int {
        return smsMessages.size
    }

     fun setOnclickListener( onItemClick: (Int) -> Unit){
         this.onItemClick = onItemClick
     }

     fun setStartViewLog(onStartViewLog: (Int) -> Unit){
         this.onStartViewList = onStartViewLog
     }

     fun setStartAckList(onStartList: (Int) -> Unit){
         this.onStartAckList = onStartList
     }

     fun setSms(sms: List<SmsObj>) {
        this.smsMessages = sms
        notifyDataSetChanged()
    }

    inner class NoteHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        var smsContent: TextView = itemView.findViewById(R.id.smsPart)
        var timeStamp: TextView = itemView.findViewById(R.id.timeStampt)
        var card: RelativeLayout = itemView.findViewById(R.id.histView)
        var separator: LinearLayout = itemView.findViewById(R.id.separator)
        var imgProfile: ImageView = itemView.findViewById(R.id.imgProfile)
        var imgAck: ImageView = itemView.findViewById(R.id.imgAck)

    }
}