package com.nemo.stwatch.adapter


import android.widget.TextView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import com.nemo.stwatch.R
import com.nemo.stwatch.db.entity.SmsObj

class SmsAdapter : androidx.recyclerview.widget.RecyclerView.Adapter<SmsAdapter.NoteHolder>() {
    private var smsMessages: List<SmsObj> = ArrayList()
    lateinit var onItemClick: (Int) -> Unit

    lateinit var onAckClick: (Int, String) -> Unit
    lateinit var onStartAckList: (Int) -> Unit
    lateinit var onStartViewList: (Int) -> Unit


     fun getMessages(): List<SmsObj>{
         return smsMessages
     }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.devices_item, parent, false)
        return NoteHolder(itemView)
    }

    override fun onBindViewHolder(holder: NoteHolder, position: Int) {
        val currentNote = smsMessages[position]
        holder.deviceId.text = currentNote.hwID
        holder.smsContent.text = currentNote.msg
        holder.timeStamp.text = currentNote.timestamp
        holder.imgViewLog.setOnClickListener {
            onStartViewList(position)
        }

        holder.imgAck.setOnClickListener {
            currentNote.isAcked = true
            onStartAckList(position)
        }

        if(smsMessages[position].isAcked){
            holder.butAck.isClickable = false
            holder.butAck.isEnabled = false
            holder.butAck.setBackgroundResource(R.drawable.border_button_grey)
        }else{

            holder.butAck.isClickable = true
            holder.butAck.isEnabled = true
            holder.butAck.setOnClickListener{
                onAckClick(position, smsMessages[position].hwID)
            }
            holder.butAck.setBackgroundResource(R.drawable.border_button_green)
        }

        holder.card.setOnClickListener{
            onItemClick(position)
        }
    }

    override fun getItemCount(): Int {
        return smsMessages.size
    }

    fun setStartViewLog(onStartViewLog: (Int) -> Unit){
        this.onStartViewList = onStartViewLog
    }

    fun setStartAckList(onStartList: (Int) -> Unit){
        this.onStartAckList = onStartList
    }

     fun setOnclickListener( onItemClick: (Int) -> Unit){
         this.onItemClick = onItemClick
     }

    fun setAckListener(onAckClick: (Int, String) -> Unit){
        this.onAckClick = onAckClick
    }

    fun setNotes(notes: List<SmsObj>) {
        this.smsMessages = notes
        notifyDataSetChanged()
    }

    inner class NoteHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        var deviceId: TextView = itemView.findViewById(R.id.deviceId)
        var smsContent: TextView = itemView.findViewById(R.id.smsPart)
        var timeStamp: TextView = itemView.findViewById(R.id.timeStampt)
        var card: androidx.cardview.widget.CardView = itemView.findViewById(R.id.carView)
        var butAck: Button = itemView.findViewById(R.id.butAck)
        var imgAck: ImageView = itemView.findViewById(R.id.imgSmsAck)
        var imgViewLog: ImageView = itemView.findViewById(R.id.imgViewLog)
    }
}