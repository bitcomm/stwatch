package com.nemo.stwatch.adapter


import android.util.Log
import android.widget.TextView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import com.nemo.stwatch.R
import com.nemo.stwatch.constants.AppConstant
import com.nemo.stwatch.response.ViewAckItemResponse
import com.nemo.stwatch.utils.TimeAgo


class AckedLogAdapter : androidx.recyclerview.widget.RecyclerView.Adapter<AckedLogAdapter.NoteHolder>() {
    private var smsMessages: List<ViewAckItemResponse> = ArrayList()
    lateinit var onItemClick: (Int) -> Unit
    private var type = AppConstant.TYPE_ACK

     fun getMessages(): List<ViewAckItemResponse>{
         return smsMessages
     }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteHolder {

         //= LayoutInflater.from(parent.context).inflate(R.layout.ack_item, parent, false)
        if(type == AppConstant.TYPE_ACK){
            var itemView = LayoutInflater.from(parent.context).inflate(R.layout.ack_item, parent, false)
            return NoteHolder(itemView)
        }else{
            var itemView = LayoutInflater.from(parent.context).inflate(R.layout.view_item, parent, false)
            return NoteHolder(itemView)
        }
    }

    override fun onBindViewHolder(holder: NoteHolder, position: Int) {
        val currentNote = smsMessages[position]

        if(currentNote.name.isNullOrBlank()){
            holder.ackName.text = "(${currentNote.mob})"
        }else{
            holder.ackName.text = "${currentNote.name} (${currentNote.mob})"
        }

        if(position == smsMessages.size - 1){
            holder.separator.visibility = View.GONE
        }else{
            holder.separator.visibility = View.VISIBLE
        }

        if(type == AppConstant.TYPE_ACK){
            holder.ackViewIcon.setBackgroundResource(R.drawable.ic_green)
        }else{
            holder.ackViewIcon.setBackgroundResource(R.drawable.ic_profile)
        }

        holder.ackTime.text = currentNote.timestamp

        var ago = TimeAgo()

        holder.ackAgo.text = ago.getTimeAgo(ago.convertDateToLong(currentNote.timestamp!!))

        holder.ackItem.setOnClickListener{
            onItemClick(position)
        }
    }

    override fun getItemCount(): Int {
        return smsMessages.size
    }

     fun setOnclickListener( onItemClick: (Int) -> Unit){
         this.onItemClick = onItemClick
     }

    fun setType(type: Int){
        this.type = type
    }

    fun setNotes(notes: List<ViewAckItemResponse>) {
        this.smsMessages = notes
        notifyDataSetChanged()
    }

    inner class NoteHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        var ackName: TextView = itemView.findViewById(R.id.tvAckViewName)
        var ackTime: TextView = itemView.findViewById(R.id.tvAckViewTime)
        var ackViewIcon: ImageView = itemView.findViewById(R.id.icImg)
        var ackAgo: TextView = itemView.findViewById(R.id.tvAckViewAgo)
        var ackItem: LinearLayout = itemView.findViewById(R.id.ackViewItemView)
        var separator: LinearLayout = itemView.findViewById(R.id.llSeparate)
    }
}