package com.nemo.stwatch.viewmodel

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nemo.stwatch.db.entity.SmsObj
import com.nemo.stwatch.repository.SmsRepository
import com.nemo.stwatch.response.AckResponse
import com.nemo.stwatch.response.OTPResponse
import com.nemo.stwatch.response.SmsResponse


class SmsViewModel(private var repository: SmsRepository) : ViewModel() {

    private var allSms: MutableLiveData<SmsResponse> = repository.getAllSms()//repository.getAllAcks()
    private var ackViewSms =  MutableLiveData<AckResponse>()
    private var logoutResponse =  MutableLiveData<OTPResponse>()

    private lateinit var allHistorySms: LiveData<List<SmsObj>>

    fun insert(note: SmsObj) {
        repository.insert(note)
    }

    fun ackViewSms(hwId: String, mobileNumber: String, timeStamp: String, type: Int){
        ackViewSms = repository.ackViewSms(hwId, mobileNumber, timeStamp, type)
    }

    fun logout(){
        logoutResponse = repository.logout()
    }

    fun getLogoutResponse(): LiveData<OTPResponse>{
        return logoutResponse
    }

    fun getAcks(): LiveData<AckResponse>{
        return ackViewSms
    }

    fun getLatest(){
        allSms = repository.getLatestSmsFromCloud()
    }

    fun getHistorySmsFromCloud(context: Context, phoneNumber: String, deviceId: String, lastTime: String){
        allSms = repository.getHistorySmsFromCloud(phoneNumber, deviceId, lastTime)
    }

    fun getLatestSms(): MutableLiveData<SmsResponse> {
        return allSms
    }

}