package com.nemo.stwatch.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.nemo.stwatch.repository.LoginRepository
import com.nemo.stwatch.response.LoginResponse
import com.nemo.stwatch.response.OTPAuthenResponse
import com.nemo.stwatch.response.OTPResponse
import io.reactivex.disposables.Disposable


class LoginViewModel : ViewModel() {

    private lateinit var loginResponseData: LiveData<LoginResponse>// = repository.getAllAcks()
    private var otpResponseData = MutableLiveData<OTPResponse>()
    private var otpAuthenResponseData = MutableLiveData<OTPAuthenResponse>()

    private lateinit var allHistorySms: LiveData<LoginResponse>

    private lateinit var subscription: Disposable



    fun getOtp(deviceId: String, phoneNumber: String){
        var repository = LoginRepository()
        otpResponseData = repository.getOTP(deviceId, phoneNumber)
    }

    fun sendLogin(requestId: String, otpCode: String){
        var repository = LoginRepository()
        otpAuthenResponseData = repository.authenOTP(requestId, otpCode)
    }

    fun getLoginResponse(): MutableLiveData<OTPResponse> {
        return otpResponseData
    }

    fun getAuthenResponse(): MutableLiveData<OTPAuthenResponse>{
        return otpAuthenResponseData
    }

}