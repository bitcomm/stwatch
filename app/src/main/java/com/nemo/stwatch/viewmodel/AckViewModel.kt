package com.nemo.stwatch.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nemo.stwatch.db.entity.SmsObj
import com.nemo.stwatch.repository.AckViewedRepository
import com.nemo.stwatch.response.ViewAckResponse


class AckViewModel : ViewModel() {

    private var ackList =  MutableLiveData<ViewAckResponse>()

    private lateinit var allHistorySms: LiveData<List<SmsObj>>


    fun getAckList(hwId: String, timeStamp: String, type: Int ){
        var repository = AckViewedRepository()
        ackList = repository.getAckList(hwId, timeStamp, type)
    }


    fun getAllAcks(): LiveData<ViewAckResponse> {
        return ackList
    }
}