package com.nemo.stwatch.networkservice

import android.text.TextUtils
import android.util.Base64
import android.util.Log
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.nemo.stwatch.constants.APIConstant
import com.nemo.stwatch.constants.AppConstant
import com.nemo.stwatch.request.LoginRequest
import com.nemo.stwatch.response.*
import com.nemo.stwatch.utils.AppSingleton
import okhttp3.Interceptor
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Query
import java.io.IOException
import java.net.SocketTimeoutException


interface RetrofitService {

    @POST(APIConstant.LOGIN)
    fun login(@Body loginRequest: LoginRequest): Call<LoginResponse>

    @GET(APIConstant.ARTICLES)
    fun getArticles(): Call<LoginResponse>

    @GET(APIConstant.GET_OTP)
    fun getOTP(@Query("device-id") deviceId: String, @Query("mobile-number") mobileNumber: String, @Query("phone-model")deviceModel: String): Call<OTPResponse>

    @GET(APIConstant.LOGOUT)
    fun logout(): Call<OTPResponse>

    @GET(APIConstant.GET_LATEST_SMS)
    fun getLatestSMS(): Call<SmsResponse>

    @GET(APIConstant.GET_HISTORY_SMS)
    fun getHistorySMS(@Query("mobile-number") requestId: String, @Query("hardware-id")hardwareId: String, @Query("lasttime")lastTime: String): Call<SmsResponse>

    @GET(APIConstant.CONFIRM_OTP)
    fun authenOTP(@Query("request-id") requestId: String, @Query("value") otp: String, @Query("mobile-number")mobileNumber: String): Call<OTPAuthenResponse>

    @GET(APIConstant.ACK_SMS)//?hardware-id=201B&mobile-number=999555891&timestamp=20191115091626&type=1
    fun ackSms(@Query("hardware-id") hardwareId: String, @Query("mobile-number") mobileNumber: String, @Query("timestamp")timestamp: String, @Query("type") type: Int) : Call<AckResponse>

    @GET(APIConstant.GET_ACK_LIST)//?hardware-id=201B&timestamp=20191115091626&type=1
    fun getAckViewList(@Query("hardware-id")hardwareId: String, @Query("timestamp")timestamp: String, @Query("type")type: Int) : Call<ViewAckResponse>


    companion object Factory {

        private val builder = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(APIConstant.BASE_API)

        private var retrofit = builder.build()
        private val httpClient = OkHttpClient.Builder()

        fun <S> createService(
            serviceClass: Class<S>, username: String?, password: String?
        ): S {
            if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {

                val credentials = username + ":" + password
                val auth = "Basic " + Base64.encodeToString(
                    credentials.toByteArray(),
                    Base64.NO_WRAP
                )

                return createService(serviceClass, auth)
            }

            return createService(serviceClass, null)
        }

        @Throws(IOException::class)
        private fun onOnIntercept(chain: Interceptor.Chain): Response {
            try {

                val response = chain.proceed(chain.request())
                val content =
                    response.body()?.string()//UtilityMethods.convertResponseToString(response)
            } catch (exception: SocketTimeoutException) {
                exception.printStackTrace()
            }

            return chain.proceed(chain.request())
        }

        fun <S> createService(serviceClass: Class<S>, authToken: String?): S {

            httpClient.addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY// else HttpLoggingInterceptor.Level.NONE
            })

            httpClient.addInterceptor(Interceptor {
                val newRequest = it.request().newBuilder()
                    .addHeader("X-Auth-Token", AppSingleton.deviceToken)
                    .addHeader("Mob", AppSingleton.mob)
                    .build()

                it.proceed(newRequest)
            })

            builder.client(httpClient.build())
            retrofit = builder.build()

            return retrofit.create(serviceClass)
        }
    }

}