package com.nemo.stwatch.db


import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import android.content.Context
import android.os.AsyncTask
import com.nemo.stwatch.db.dao.SmsDao
import com.nemo.stwatch.db.entity.SmsObj

@Database(entities = [SmsObj::class], version = 1)
abstract class SmsDatabase : RoomDatabase() {

    abstract fun smsDao(): SmsDao
    companion object {
        private var instance: SmsDatabase? = null

        fun getInstance(context: Context): SmsDatabase {
            if (instance == null) {
                synchronized(SmsDatabase::class) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        SmsDatabase::class.java, "stwatch_database"
                    )
                        .fallbackToDestructiveMigration()
                        .addCallback(roomCallback)
                        .build()
                }
            }
            return instance!!
        }

        fun destroyInstance() {
            instance = null
        }

        private val roomCallback = object : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                PopulateDbAsyncTask(instance)
                    .execute()
            }
        }

    }
    class PopulateDbAsyncTask(db: SmsDatabase?) : AsyncTask<Unit, Unit, Unit>() {
        private val noteDao = db?.smsDao()
        override fun doInBackground(vararg p0: Unit?) {
        }
    }
}