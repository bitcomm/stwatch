package com.nemo.stwatch.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "sms_table")
data class SmsObj(

    var mob: String,
    var hwID: String,
    var msg: String,
    var timestamp: String,
    var createdAt: String,
    var isAcked: Boolean,
    var isViewed: Boolean
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}