package com.nemo.stwatch.db.dao

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.*
import com.nemo.stwatch.db.entity.SmsObj

@Dao
interface SmsDao {

    @Insert
    fun insert(note: SmsObj)

    @Update
    fun update(smsObj: SmsObj)

    @Query("DELETE FROM sms_table")
    fun deleteAllSms()

    @Query("SELECT * FROM sms_table ")
    fun getAllSms(): LiveData<List<SmsObj>>

    @Query("SELECT * FROM sms_table WHERE hwID ==:hwID")
    fun getAllSmsByDevice(hwID: String): LiveData<List<SmsObj>>
}