package com.nemo.stwatch

import android.app.Application
import android.util.Log
import com.nemo.stwatch.di.dbModule
import com.nemo.stwatch.di.repositoryModule
import com.nemo.stwatch.di.uiModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import android.app.Activity
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T


class CoreApplication: Application() {


    override fun onCreate() {
        super.onCreate()
        Log.e("TAG", "onCreate")
        if (!BuildConfig.DEBUG) {
            Fabric.with(this, Crashlytics())
        }
        startKoin {
            androidContext(this@CoreApplication)
            modules(listOf(dbModule, repositoryModule, uiModule))
        }
    }
}