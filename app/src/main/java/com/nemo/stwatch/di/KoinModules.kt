package com.nemo.stwatch.di

import com.nemo.stwatch.adapter.AckedLogAdapter
import com.nemo.stwatch.adapter.SmsAdapter
import com.nemo.stwatch.db.SmsDatabase
import com.nemo.stwatch.repository.LoginRepository
import com.nemo.stwatch.repository.SmsRepository
import com.nemo.stwatch.viewmodel.AckViewModel
import com.nemo.stwatch.viewmodel.LoginViewModel
import com.nemo.stwatch.viewmodel.SmsViewModel
import layout.SmsHistoryAdapter
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val dbModule = module {
    single { SmsDatabase.getInstance(
        context = get()
    )}
    factory { get<SmsDatabase>().smsDao() }
}

val repositoryModule = module {
    single { SmsRepository(get()) }
    single { LoginRepository()}
}

val uiModule = module {
    factory { SmsAdapter()}
    factory { AckedLogAdapter() }
    factory { SmsHistoryAdapter() }
    viewModel { SmsViewModel(get())}
    viewModel { AckViewModel() }
    viewModel {LoginViewModel()}
}

