package com.nemo.stwatch.activities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.lifecycle.Observer
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.nemo.stwatch.R
import com.nemo.stwatch.constants.APIConstant
import com.nemo.stwatch.constants.AppConstant
import com.nemo.stwatch.utils.PreferenceUtils
import com.nemo.stwatch.utils.UiUtils
import com.nemo.stwatch.viewmodel.SmsViewModel
import kotlinx.android.synthetic.main.activity_main.*
import layout.SmsHistoryAdapter
import org.koin.android.ext.android.inject
import java.text.SimpleDateFormat
import java.util.*


class HistoryActivity : BaseActivity() {

    val TAG = "HistoryActivity"
    private val smsViewModel: SmsViewModel by inject()
    private val smsAdapter: SmsHistoryAdapter by inject()

    lateinit var deviceId: String
    lateinit var lastedTime: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)

        var toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        //home navigation
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        setupRecyclerView()

        deviceId = intent.getStringExtra(AppConstant.KEY_INTENT_DEVICE_ID)
        lastedTime = intent.getStringExtra(AppConstant.KEY_INTENT_LASTED_TIME)

        lastedTime = lastedTime.replace("T", "")
        lastedTime = lastedTime.replace("+", "")
        lastedTime = lastedTime.substring(0, lastedTime.length - 5)
        lastedTime = lastedTime.replace("-", "")
        lastedTime = lastedTime.replace(":", "")

        supportActionBar?.title = "$deviceId History"
        toolbar.title = "$deviceId History"

        smsAdapter.setOnclickListener {
            //Log.e(TAG, "Nemo ${it} ${smsAdapter.getMessages()[it].hwID}")

        }

        smsAdapter.setStartAckList {
            var intent = Intent(this, ViewedAckActivity::class.java)
            intent.putExtra(AppConstant.KEY_INTENT_ACTION_TYPE, AppConstant.TYPE_ACK)
            intent.putExtra(AppConstant.KEY_INTENT_DEVICE_ID, smsAdapter.getMessages()[it].hwID)
            intent.putExtra(AppConstant.KEY_INTENT_MESSAGE, smsAdapter.getMessages()[it].msg)
            intent.putExtra(AppConstant.KEY_INTENT_LASTED_TIME, smsAdapter.getMessages()[it].timestamp)
            intent.putExtra(AppConstant.KEY_INTENT_MOB, smsAdapter.getMessages()[it].mob)
            startActivity(intent)
        }

        smsAdapter.setStartViewLog {
            var intent = Intent(this, ViewedAckActivity::class.java)
            intent.putExtra(AppConstant.KEY_INTENT_ACTION_TYPE, AppConstant.TYPE_VIEW)
            intent.putExtra(AppConstant.KEY_INTENT_DEVICE_ID, smsAdapter.getMessages()[it].hwID)
            intent.putExtra(AppConstant.KEY_INTENT_MESSAGE, smsAdapter.getMessages()[it].msg)
            intent.putExtra(AppConstant.KEY_INTENT_LASTED_TIME, smsAdapter.getMessages()[it].timestamp)
            intent.putExtra(AppConstant.KEY_INTENT_MOB, smsAdapter.getMessages()[it].mob)
            startActivity(intent)
        }

        val localBroadcastManager = LocalBroadcastManager.getInstance(this)

        localBroadcastManager.registerReceiver(object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                callRequest()
            }
            // defined in ServerService below
        }, IntentFilter(APIConstant.RECEIVER_ACTION))
    }

    override fun onResume() {
        super.onResume()
        callRequest()
    }

    private fun callRequest(){
        smsViewModel.getHistorySmsFromCloud(this, PreferenceUtils.getStringPref(this, AppConstant.KEY_INTENT_PHONE_NUMBER, ""), deviceId, lastedTime)
        smsViewModel.getLatestSms().observe(this, Observer{ list->
            if(list.statusCode == 401){
                UiUtils.forceLogoutDialog(this)
            }else {
                if (!list.data.isNullOrEmpty()) {
                    val sdf = SimpleDateFormat(AppConstant.DATE_FORMAT, Locale.ENGLISH)
                    var sortList = list.data!!.sortedByDescending { sdf.parse(it.createdAt)  }//list.sortedWith(compareBy { (sdf.parse(it.createdAt)) })

                    smsAdapter.setSms(sortList)
                }
            }
        })
    }


    private fun setupRecyclerView() {
        recycler_view.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recycler_view.setHasFixedSize(true)
        recycler_view.adapter = smsAdapter
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }

            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    companion object{
        private const val ADD_NOTE_REQUEST = 1
    }
}