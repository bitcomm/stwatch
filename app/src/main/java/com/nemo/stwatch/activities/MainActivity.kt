package com.nemo.stwatch.activities

import android.app.Activity
import android.content.*
import androidx.lifecycle.Observer
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import com.nemo.stwatch.R
import com.nemo.stwatch.adapter.SmsAdapter
import com.nemo.stwatch.constants.AppConstant
import com.nemo.stwatch.db.entity.SmsObj
import com.nemo.stwatch.viewmodel.SmsViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.LinearLayoutManager
import com.nemo.stwatch.utils.PreferenceUtils
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.nemo.stwatch.constants.APIConstant
import com.nemo.stwatch.response.AckResponse
import com.nemo.stwatch.response.OTPResponse
import com.nemo.stwatch.utils.UiUtils
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : BaseActivity() {

    val TAG = "MainActivity"
    private val smsViewModel: SmsViewModel by inject()
    private val smsAdapter: SmsAdapter by inject()

    var lastVisibleItem: Int = 0
    var totalItemCount: Int = 0

    var firstVisibleCount = 0

    override fun onResume() {
        super.onResume()

        callRequest()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupRecyclerView()

        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.title = resources.getText(R.string.label_agc_summary)

        smsAdapter.setStartViewLog {
            var intent = Intent(this, ViewedAckActivity::class.java)
            intent.putExtra(AppConstant.KEY_INTENT_ACTION_TYPE, AppConstant.TYPE_VIEW)
            intent.putExtra(AppConstant.KEY_INTENT_DEVICE_ID, smsAdapter.getMessages()[it].hwID)
            intent.putExtra(AppConstant.KEY_INTENT_MESSAGE, smsAdapter.getMessages()[it].msg)
            intent.putExtra(AppConstant.KEY_INTENT_CREATEAT, smsAdapter.getMessages()[it].createdAt)
            intent.putExtra(AppConstant.KEY_INTENT_LASTED_TIME, smsAdapter.getMessages()[it].timestamp)
            intent.putExtra(AppConstant.KEY_INTENT_MOB, smsAdapter.getMessages()[it].mob)
            startActivity(intent)
        }

        smsAdapter.setStartAckList {
            var intent = Intent(this, ViewedAckActivity::class.java)
            intent.putExtra(AppConstant.KEY_INTENT_ACTION_TYPE, AppConstant.TYPE_ACK)
            intent.putExtra(AppConstant.KEY_INTENT_DEVICE_ID, smsAdapter.getMessages()[it].hwID)
            intent.putExtra(AppConstant.KEY_INTENT_MESSAGE, smsAdapter.getMessages()[it].msg)
            intent.putExtra(AppConstant.KEY_INTENT_CREATEAT, smsAdapter.getMessages()[it].createdAt)
            intent.putExtra(AppConstant.KEY_INTENT_LASTED_TIME, smsAdapter.getMessages()[it].timestamp)
            intent.putExtra(AppConstant.KEY_INTENT_MOB, smsAdapter.getMessages()[it].mob)
            startActivity(intent)
        }

        smsAdapter.setOnclickListener {
            var intent = Intent(this, HistoryActivity::class.java)
            intent.putExtra(AppConstant.KEY_INTENT_DEVICE_ID, smsAdapter.getMessages()[it].hwID)
            intent.putExtra(AppConstant.KEY_INTENT_LASTED_TIME, smsAdapter.getMessages()[it].createdAt)
            startActivity(intent)
        }

        smsAdapter.setAckListener{ i: Int, s: String ->

            smsViewModel.ackViewSms(s, smsAdapter.getMessages()[i].mob, smsAdapter.getMessages()[i].timestamp, AppConstant.TYPE_ACK)
            smsViewModel.getAcks().observe(this, Observer<AckResponse>{
                callRequest()
            })

            smsAdapter.getMessages()[i].isAcked = true
        }

        val localBroadcastManager = LocalBroadcastManager.getInstance(this)

        localBroadcastManager.registerReceiver(object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                callRequest()
            }
            // defined in ServerService below
        }, IntentFilter(APIConstant.RECEIVER_ACTION))
    }

    private fun callRequest(){
        smsViewModel.getLatest()
        smsViewModel.getLatestSms().observe(this, Observer{ list->

            if(list.statusCode == 401){
                UiUtils.forceLogoutDialog(this)
            }else{
                if(list.data != null && list.data!!.isNotEmpty()){
                    val sdf = SimpleDateFormat(AppConstant.DATE_FORMAT, Locale.ENGLISH)
                    var sortList = list.data!!.sortedByDescending { sdf.parse(it.createdAt)  }//list.sortedWith(compareBy { (sdf.parse(it.createdAt)) })

                    smsAdapter.setNotes(sortList)
                }
            }
        })
    }

    private fun setupRecyclerView() {

        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.setHasFixedSize(true)
        recycler_view.adapter = smsAdapter

        recycler_view.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val linearLayoutManager = recycler_view.layoutManager as LinearLayoutManager

                totalItemCount = linearLayoutManager.itemCount
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition()

                for(i in 0 until linearLayoutManager.findLastVisibleItemPosition()){
                    var smsItem = smsAdapter.getMessages()[i]
                    setViewedByUser(smsItem)
                }


                if(firstVisibleCount < linearLayoutManager.findLastVisibleItemPosition()){
                    var smsItem = smsAdapter.getMessages()[linearLayoutManager.findLastVisibleItemPosition()]
                    setViewedByUser(smsItem)
                }
            }
        })
    }

    fun setViewedByUser(smsItem: SmsObj){
        if(!smsItem.isViewed){
            smsItem.isViewed = true
            smsViewModel.ackViewSms(smsItem.hwID, smsItem.mob, smsItem.timestamp, AppConstant.TYPE_VIEW)
            smsViewModel.getAcks().observe(this, Observer<AckResponse>{
            })
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(com.nemo.stwatch.R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }

            R.id.logout -> {
                logoutDialog(this@MainActivity)

                return true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    fun logoutDialog(activity: Activity){
        val builder = AlertDialog.Builder(activity)
        builder.setTitle(activity.resources.getText(R.string.label_logout))
        builder.setPositiveButton(activity.resources.getText(R.string.action_yes)){dialog, which ->

            smsViewModel.logout()
            smsViewModel.getLogoutResponse().observe(this, Observer<OTPResponse>{
                if(it.statusCode == 200){
                    PreferenceUtils.saveBooleanPref(activity, AppConstant.KEY_INTENT_IS_LOGINED, false)
                    activity.startActivity(Intent(activity, LoginActivity::class.java))
                    activity.finish()
                }
            })

        }

        builder.setNegativeButton(activity.resources.getText(R.string.action_no)){dialog, which ->
        }

        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()
        dialog.setCancelable(false)

        // Display the alert dialog on app interface
        dialog.show()
    }
}