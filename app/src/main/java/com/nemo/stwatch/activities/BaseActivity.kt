package com.nemo.stwatch.activities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.nemo.stwatch.constants.APIConstant
import com.nemo.stwatch.constants.AppConstant
import com.nemo.stwatch.utils.UiUtils


open class BaseActivity: AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val localBroadcastManager = LocalBroadcastManager.getInstance(this)

        localBroadcastManager.registerReceiver(object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {

                var message = intent.getStringExtra(AppConstant.KEY_INTENT_MESSAGE)

                UiUtils.warningLoginAlready(this@BaseActivity, message)
            }
            // defined in ServerService below
        }, IntentFilter(APIConstant.RECEIVER_LOGOUT))
    }
}