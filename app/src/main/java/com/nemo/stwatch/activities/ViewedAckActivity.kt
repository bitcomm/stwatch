package com.nemo.stwatch.activities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.lifecycle.Observer
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.nemo.stwatch.R
import com.nemo.stwatch.adapter.AckedLogAdapter
import com.nemo.stwatch.constants.APIConstant
import com.nemo.stwatch.constants.AppConstant
import com.nemo.stwatch.utils.UiUtils
import com.nemo.stwatch.viewmodel.AckViewModel
import kotlinx.android.synthetic.main.activity_ack_log.*
import org.koin.android.ext.android.inject
import java.text.SimpleDateFormat
import java.util.*


class ViewedAckActivity : BaseActivity() {

    val TAG = "ViewedAckActivity"
    private val smsViewModel: AckViewModel by inject()
    private val smsAdapter: AckedLogAdapter by inject()

    lateinit var deviceId: String
    lateinit var lastedTime: String
    var actionType: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ack_log)

        var toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        //home navigation
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        setupRecyclerView()

        deviceId = intent.getStringExtra(AppConstant.KEY_INTENT_DEVICE_ID)
        lastedTime = intent.getStringExtra(AppConstant.KEY_INTENT_LASTED_TIME)
        actionType = intent.getIntExtra(AppConstant.KEY_INTENT_ACTION_TYPE, 0)

        smsMessage.text = intent.getStringExtra(AppConstant.KEY_INTENT_MESSAGE)
        smsTime.text = lastedTime

        if(actionType == AppConstant.TYPE_ACK){
            supportActionBar?.title = getString(R.string.label_event_ack_log, deviceId)//"$deviceId History"
            toolbar.title = getString(R.string.label_event_ack_log, deviceId)//"$deviceId History"
            tvTitleList.text = getString(R.string.label_event_ack_by)
        }else{
            supportActionBar?.title = getString(R.string.label_event_view_log, deviceId)//"$deviceId History"
            toolbar.title = getString(R.string.label_event_view_log, deviceId)//"$deviceId History"
            tvTitleList.text = getString(R.string.label_viewed_by)
        }

        smsAdapter.setType(actionType)
        smsAdapter.setOnclickListener {
//            Log.e(TAG, "Nemo ${it} ${smsAdapter.getMessages()[it].hwID}")
        }

        val localBroadcastManager = LocalBroadcastManager.getInstance(this)

        localBroadcastManager.registerReceiver(object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                callRequest()
            }
            // defined in ServerService below
        }, IntentFilter(APIConstant.RECEIVER_ACTION))
    }

    override fun onResume() {
        super.onResume()
        callRequest()
    }

    private fun callRequest(){
        smsViewModel.getAckList(deviceId, lastedTime, actionType)
        smsViewModel.getAllAcks().observe(this, Observer{ data->

            if(data != null && data.data!!.size > 0){
                data.data?.let { smsAdapter.setNotes(it) }
            }else{
                tvNoData.visibility = View.VISIBLE
            }
        })
    }


    private fun setupRecyclerView() {
        ackList.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        ackList.setHasFixedSize(true)
        ackList.adapter = smsAdapter
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }

            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    companion object{
        private const val ADD_NOTE_REQUEST = 1
    }
}