package com.nemo.stwatch.activities

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.iid.FirebaseInstanceId
import com.nemo.stwatch.R
import com.nemo.stwatch.viewmodel.LoginViewModel
import androidx.lifecycle.Observer
import com.nemo.stwatch.BuildConfig
import com.nemo.stwatch.constants.AppConstant
import com.nemo.stwatch.response.OTPAuthenResponse
import com.nemo.stwatch.utils.AppSingleton
import com.nemo.stwatch.utils.PreferenceUtils
import kotlinx.android.synthetic.main.login_main.*
import org.koin.android.ext.android.inject
import java.lang.Long.parseLong

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    var TAG = "LoginActivity"
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private val loginViewModel: LoginViewModel by inject()
    private var deviceId: String = ""
    private var requestId: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_main)

        loadDeviceInfo()
        initFirebase()


        if(BuildConfig.DEBUG){
            edt_username.setText("9156341904")
        }

        if(PreferenceUtils.getBooleanPref(this, AppConstant.KEY_INTENT_IS_LOGINED, false)) {
            AppSingleton.deviceToken = PreferenceUtils.getStringPref(this, AppConstant.PREF_TOKEN, "")
            AppSingleton.mob = PreferenceUtils.getStringPref(this, AppConstant.PREF_MOB, "")
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }

        edt_username.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                try {
                    val num = parseLong(p0.toString())

                    if(num.toString().length > 10){
                        Toast.makeText(this@LoginActivity, R.string.phone_is_too_long, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: NumberFormatException) {
                    Toast.makeText(this@LoginActivity, R.string.invalid_phone, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun loadDeviceInfo(){
        //Log.e(TAG, "loadDeviceInfo ${HelpUtils.getDeviceName()}")
    }

    fun initFirebase(){
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)

        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    //Log.w(TAG, "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }
                // Get new Instance ID token
                val token = task.result?.token
                Log.d(TAG, token)
                deviceId = token.toString()

                AppSingleton.deviceToken = deviceId
                PreferenceUtils.saveStringPref(this, AppConstant.PREF_TOKEN, deviceId)
            })
    }

    fun loginClick(v: View) {
        if(requestId.isEmpty() || edt_password.text.isEmpty()){
            Toast.makeText(applicationContext, resources.getText(R.string.toast_fille_otp), Toast.LENGTH_SHORT).show()
        }else{
            loginViewModel.sendLogin(requestId, edt_password.text.toString())
            loginViewModel.getAuthenResponse().observe(this, Observer<OTPAuthenResponse> { data -> data?.let{
                if(data.statusCode == 200){
                    startActivity(Intent(this, MainActivity::class.java))
                    PreferenceUtils.saveBooleanPref(this, AppConstant.KEY_INTENT_IS_LOGINED, true)
                    PreferenceUtils.saveStringPref(this, AppConstant.KEY_INTENT_PHONE_NUMBER, edt_username.text.toString())
                }else{
                    Toast.makeText(this, data.message, Toast.LENGTH_SHORT).show()
                }
            }  })
        }
    }

    fun sendOtpClick(v: View) {
        if(edt_username.text.isEmpty()){
            Toast.makeText(applicationContext, resources.getText(R.string.toast_fill_phone_number), Toast.LENGTH_SHORT).show()
        }else{
            tvSendOtp.setTextColor(Color.GRAY)
            tvSendOtp.isEnabled = false
            tvSendOtp.isClickable = false
            tvSendOtp.text = getString(R.string.sending)

            loginViewModel.getOtp(deviceId, edt_username.text.toString())
            loginViewModel.getLoginResponse().observe(this, Observer{data->
                tvSendOtp.isEnabled = true
                tvSendOtp.isClickable = true
                tvSendOtp.text = getString(R.string.send_otp)
                tvSendOtp.setTextColor(resources.getColor(R.color.blue_light_color))

                if(data.data == null){
                    Toast.makeText(applicationContext, data.message, Toast.LENGTH_SHORT).show()
                }else{
                    AppSingleton.mob = edt_username.text.toString()
                    PreferenceUtils.saveStringPref(this, AppConstant.PREF_MOB, edt_username.text.toString().trim())
                    requestId = data.data!!.requestID!!

                    tvNotiSenTOTP.visibility = View.VISIBLE
                    tvNotiSenTOTP.text = getString(R.string.noti_sent_otp, edt_username.text.toString())
                }
            })
        }
    }


    override fun onClick(p0: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
