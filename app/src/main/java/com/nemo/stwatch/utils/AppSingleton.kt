package com.nemo.stwatch.utils

import android.os.Build

object AppSingleton{

    init {
        println("Singleton class invoked.")
    }
    var deviceToken = ""
    var mob = ""

    fun getDeviceName(): String{

        var manufacture = Build.MANUFACTURER
        var model = Build.MODEL

        if(model.startsWith(manufacture)){
            return model
        }
        return manufacture + " " + model
    }
}