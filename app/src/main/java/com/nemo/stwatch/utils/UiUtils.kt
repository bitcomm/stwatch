package com.nemo.stwatch.utils

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AlertDialog
import com.nemo.stwatch.R
import com.nemo.stwatch.activities.LoginActivity
import com.nemo.stwatch.constants.AppConstant

class UiUtils {
    companion object{

        fun warningLoginAlready(activity: Activity, message: String){
            val builder = AlertDialog.Builder(activity)
            builder.setTitle(activity.resources.getText(R.string.label_warning))
            builder.setMessage(message)
            builder.setPositiveButton(activity.resources.getText(R.string.action_ok)){dialog, which ->
            }

            // Finally, make the alert dialog using builder
            val dialog: AlertDialog = builder.create()

            if(!activity.isDestroyed){
                dialog.show()
            }
        }

        fun forceLogoutDialog(activity: Activity){
            val builder = AlertDialog.Builder(activity)
            builder.setTitle(activity.resources.getText(R.string.label_logout))
            builder.setMessage(activity.resources.getText(R.string.message_logout))
            builder.setPositiveButton(activity.resources.getText(R.string.action_yes)){dialog, which ->
                PreferenceUtils.saveBooleanPref(activity, AppConstant.KEY_INTENT_IS_LOGINED, false)
                activity.startActivity(Intent(activity, LoginActivity::class.java))
                activity.finish()
            }

            // Finally, make the alert dialog using builder
            val dialog: AlertDialog = builder.create()
            dialog.setCancelable(false)

            // Display the alert dialog on app interface
            dialog.show()
        }
    }
}