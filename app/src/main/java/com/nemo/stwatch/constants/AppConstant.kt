package com.nemo.stwatch.constants

class AppConstant{
    companion object{

        const val READ_NUMBER = "+84584436472"
        const val ACTION_START_SERVICE = "Action_start_service"
        const val ACTION_MAIN = "action_Main"
        const val RECEIVER_ACTION = "come.nemo.receiver"

        const val KEY_INTENT_DEVICE_ID = "device_id"
        const val KEY_INTENT_ACTION_TYPE = "user_action_type"
        const val KEY_INTENT_LASTED_TIME = "lasted_time"
        const val KEY_INTENT_MOB = "mob"
        const val KEY_INTENT_NAME = "name"
        const val KEY_INTENT_MESSAGE = "msg"
        const val KEY_INTENT_CREATEAT = "create_at"
        const val KEY_INTENT_SMS_OBJECT = "sms_object"

        const val PREF_MOB = "mob"
        const val PREF_TOKEN = "token"

        const val TYPE_ACK = 2
        const val TYPE_VIEW = 1

        const val KEY_INTENT_IS_LOGINED = "is_logined"
        const val KEY_INTENT_PHONE_NUMBER = "mobile_number"

        const val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"
    }
}

class ColorConstant{
    companion object {
        const val SEPARATOR = "#E4E4E4"
    }
}