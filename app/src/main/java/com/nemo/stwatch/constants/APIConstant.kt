package com.nemo.stwatch.constants
 class APIConstant{
    companion object{
        const val BASE_API = "http://148.72.206.253:9091/"// "http://115.79.29.63:8282/"
//        const val BASE_API = "https://ancient-ape-39.telebit.io/";// "http://115.79.29.63:8282/"
        const val GET_OTP = "v1/make-otp"//?device-id=000000000&mobile-number=9958955558
        const val GET_LATEST_SMS = "v1/message/latest"
        const val GET_HISTORY_SMS = "v1/message/histories"
        const val CONFIRM_OTP = "v1/validate-otp"//?request-id=JnLKo57&value=4194
        const val ACK_SMS = "/v1/hardware/setact"//?request-id=JnLKo57&value=4194
        const val VIEW_SMS = "v1/validate-otp"//?request-id=JnLKo57&value=4194
        const val GET_VIEW_SMS = "v1/validate-otp"//?request-id=JnLKo57&value=4194
        const val GET_ACK_LIST = "/v1/hardware/activities"//?request-id=JnLKo57&value=4194
        const val  LOGIN = "api/token/"
        const val LOGOUT = "v1/logout"
        const val ARTICLES = "articles"
        const val RECEIVER_ACTION = "come.nemo.receiver"
        const val RECEIVER_LOGOUT = "com.nemo.logout"
    }
}