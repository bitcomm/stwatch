package com.nemo.stwatch.repository

import android.util.Log
import com.nemo.stwatch.networkservice.RetrofitService
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.stream.JsonReader
import com.nemo.stwatch.constants.APIConstant
import com.nemo.stwatch.request.LoginRequest
import com.nemo.stwatch.response.LoginResponse
import com.nemo.stwatch.response.OTPResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import com.jakewharton.retrofit2.adapter.rxjava2.Result.response
import com.nemo.stwatch.response.AckResponse
import com.nemo.stwatch.response.OTPAuthenResponse
import com.nemo.stwatch.utils.AppSingleton
import java.io.StringReader
import java.lang.Exception
import java.net.SocketTimeoutException


class LoginRepository {

    val TAG = "LoginRepository"

    private var restAPI: RetrofitService =
        RetrofitService.createService(RetrofitService::class.java, null)

    fun getOTP(deviceId: String, mobileNumber: String): MutableLiveData<OTPResponse> {

        val newsData = MutableLiveData<OTPResponse>()
        var call = restAPI.getOTP(deviceId, mobileNumber, AppSingleton.getDeviceName())
        call.enqueue(object : Callback<OTPResponse> {
            override fun onResponse(call: Call<OTPResponse>, response: Response<OTPResponse>?) {
                if(response?.errorBody() != null){

                    var stringReader: StringReader = StringReader(response.errorBody()!!.string())
                    var jsonReader: JsonReader = JsonReader(stringReader)

                    val smsResponse = Gson().fromJson<OTPResponse>(jsonReader, OTPResponse::class.java)
                    newsData.value = smsResponse

                }else{
                    var wResponseSmsResponse = response?.body()
                    newsData.value = wResponseSmsResponse
                }
            }

            override fun onFailure(call: retrofit2.Call<OTPResponse>, t: Throwable) {

                if(t is SocketTimeoutException){
                    newsData.value = null
                }
            }
        })

        return newsData
    }

    fun authenOTP(requestId: String, otp: String): MutableLiveData<OTPAuthenResponse> {

        val newsData = MutableLiveData<OTPAuthenResponse>()
        var call = restAPI.authenOTP(requestId, otp, AppSingleton.mob)
        call.enqueue(object : Callback<OTPAuthenResponse> {
            override fun onResponse(call: Call<OTPAuthenResponse>, response: Response<OTPAuthenResponse>?) {
                if(response?.errorBody() != null){

                    var stringReader: StringReader = StringReader(response.errorBody()!!.string())
                    var jsonReader: JsonReader = JsonReader(stringReader)

                    val smsResponse = Gson().fromJson<OTPAuthenResponse>(jsonReader, OTPAuthenResponse::class.java)
                    newsData.value = smsResponse

                }else{
                    var wResponseSmsResponse = response?.body()
                    newsData.value = wResponseSmsResponse
                }
            }

            override fun onFailure(call: retrofit2.Call<OTPAuthenResponse>, t: Throwable) {
                Log.e(TAG, "FAIL ")
            }
        })

        return newsData
    }

    fun login(username: String, password: String): MutableLiveData<LoginResponse> {
        var loginRequest = LoginRequest()

        val newsData = MutableLiveData<LoginResponse>()
        var call = restAPI.login(loginRequest)
        call.enqueue(object : Callback<LoginResponse> {
            override fun onResponse(call: retrofit2.Call<LoginResponse>, response: retrofit2.Response<LoginResponse>?) {
                if (response != null) {
                    newsData.value = response.body()
                }
            }

            override fun onFailure(call: retrofit2.Call<LoginResponse>, t: Throwable) {
                //Log.e(TAG, "FAIL ");
            }
        })
        return newsData
    }
}