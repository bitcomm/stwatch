package com.nemo.stwatch.repository

import androidx.lifecycle.LiveData
import android.os.AsyncTask
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.nemo.stwatch.db.dao.SmsDao
import com.nemo.stwatch.db.entity.SmsObj
import com.nemo.stwatch.networkservice.RetrofitService
import com.nemo.stwatch.utils.AppSingleton
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import com.jakewharton.retrofit2.adapter.rxjava2.Result.response
import android.R.string
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.stream.JsonReader
import com.nemo.stwatch.response.*
import org.json.JSONObject
import java.io.StringReader
import java.net.SocketTimeoutException


class SmsRepository(private val smsDao: SmsDao) {

    companion object{
        val TAG = "SmsRepository"
    }

    private var allSms =  MutableLiveData<SmsResponse>()

    private var restAPI: RetrofitService = RetrofitService.createService(RetrofitService::class.java, null)

    fun insert(note: SmsObj) {
        InsertNoteAsyncTask(
            smsDao
        ).execute(note)
    }

    fun logout(): MutableLiveData<OTPResponse>{

        val newsData = MutableLiveData<OTPResponse>()
        var call = restAPI.logout()
        call.enqueue(object : Callback<OTPResponse> {
            override fun onResponse(call: Call<OTPResponse>, response: Response<OTPResponse>?) {
                if(response?.errorBody() != null){

                    var stringReader: StringReader = StringReader(response.errorBody()!!.string())
                    var jsonReader: JsonReader = JsonReader(stringReader)

                    val smsResponse = Gson().fromJson<OTPResponse>(jsonReader, OTPResponse::class.java)
                    newsData.value = smsResponse

                }else{
                    var wResponseSmsResponse = response?.body()
                    newsData.value = wResponseSmsResponse
                }
            }

            override fun onFailure(call: retrofit2.Call<OTPResponse>, t: Throwable) {

                if(t is SocketTimeoutException){
                    newsData.value = null
                }
            }
        })

        return newsData
    }

    fun ackViewSms(hardId: String, mobileNumber: String, timeStamp: String, type: Int): MutableLiveData<AckResponse>{

        var lastTimeValue = timeStamp.replace("-", "").replace(" ", "").replace(":", "")

        val newsData = MutableLiveData<AckResponse>()
        var call = restAPI.ackSms(hardId, mobileNumber, lastTimeValue, type)
        call.enqueue(object : Callback<AckResponse> {
            override fun onResponse(call: Call<AckResponse>, response: Response<AckResponse>?) {

                if(response?.errorBody() != null){

                    var stringReader: StringReader = StringReader(response.errorBody()!!.string())
                    var jsonReader: JsonReader = JsonReader(stringReader)

                    val smsResponse = Gson().fromJson<AckResponse>(jsonReader, AckResponse::class.java)

                    newsData.value = smsResponse

                }else{
                    var wResponseSmsResponse = response?.body()
                    newsData.value = wResponseSmsResponse
                }
            }

            override fun onFailure(call: retrofit2.Call<AckResponse>, t: Throwable) {
            }
        })

        return newsData
    }


    fun getAllSms(): MutableLiveData<SmsResponse> {
        return allSms
    }

    private class InsertNoteAsyncTask(val noteDao: SmsDao) : AsyncTask<SmsObj, Unit, Unit>() {

        override fun doInBackground(vararg note: SmsObj?) {
            noteDao.insert(note[0]!!)
        }
    }

    fun getLatestSmsFromCloud(): MutableLiveData<SmsResponse> {

        val newsData = MutableLiveData<SmsResponse>()
        var call = restAPI.getLatestSMS()
        call.enqueue(object : Callback<SmsResponse> {
            override fun onResponse(call: Call<SmsResponse>, response: Response<SmsResponse>?) {

                if(response?.errorBody() != null){

                    var stringReader: StringReader = StringReader(response.errorBody()!!.string())
                    var jsonReader: JsonReader = JsonReader(stringReader)

                    val smsResponse = Gson().fromJson<SmsResponse>(jsonReader, SmsResponse::class.java)

                    newsData.value = smsResponse

                }else{
                    var wResponseSmsResponse = response?.body()
                    newsData.value = wResponseSmsResponse
                }
            }

            override fun onFailure(call: retrofit2.Call<SmsResponse>, t: Throwable) {
            }
        })

        return newsData
    }

    fun getHistorySmsFromCloud(phoneNumber: String, deviceId: String, lastTime: String): MutableLiveData<SmsResponse> {
        Log.e(TAG, "getHistorySmsFromCloud")

        val newsData = MutableLiveData<SmsResponse>()
        var call = restAPI.getHistorySMS(phoneNumber, deviceId, lastTime)
        call.enqueue(object : Callback<SmsResponse> {
            override fun onResponse(call: Call<SmsResponse>, response: Response<SmsResponse>?) {
                if(response?.errorBody() != null){

                    var stringReader: StringReader = StringReader(response.errorBody()!!.string())
                    var jsonReader: JsonReader = JsonReader(stringReader)

                    val smsResponse = Gson().fromJson<SmsResponse>(jsonReader, SmsResponse::class.java)

                    newsData.value = smsResponse

                }else{
                    var wResponseSmsResponse = response?.body()
                    Log.e(TAG, "RESPONSE ${wResponseSmsResponse?.statusCode}")
                    newsData.value = wResponseSmsResponse
                }
            }

            override fun onFailure(call: retrofit2.Call<SmsResponse>, t: Throwable) {
            }
        })

        return newsData
    }

}