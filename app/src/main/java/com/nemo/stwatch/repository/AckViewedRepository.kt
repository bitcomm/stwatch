package com.nemo.stwatch.repository

import androidx.lifecycle.LiveData
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.nemo.stwatch.networkservice.RetrofitService
import com.nemo.stwatch.response.ViewAckResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AckViewedRepository {

    companion object{
        val TAG = "AckViewedRepository"
    }

    private val ackListResponse =  MutableLiveData<ViewAckResponse>()

    private var restAPI: RetrofitService =
        RetrofitService.createService(RetrofitService::class.java, null)

    fun getAckList(hardId: String, timeStamp: String, type: Int): MutableLiveData<ViewAckResponse>{

        var lastTimeValue = timeStamp.replace("-", "").replace(" ", "").replace(":", "")

        val newsData = MutableLiveData<ViewAckResponse>()
        var call = restAPI.getAckViewList(hardId, lastTimeValue, type)
        call.enqueue(object : Callback<ViewAckResponse> {
            override fun onResponse(call: Call<ViewAckResponse>, response: Response<ViewAckResponse>?) {
                if (response != null) {
                    var wResponseSmsResponse = response.body()
                    newsData.value = wResponseSmsResponse
                }
            }

            override fun onFailure(call: retrofit2.Call<ViewAckResponse>, t: Throwable) {
            }
        })

        return newsData
    }

    fun getAllSms(): LiveData<ViewAckResponse> {
        return ackListResponse
    }
}