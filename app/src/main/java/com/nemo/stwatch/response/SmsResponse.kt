package com.nemo.stwatch.response

import com.google.gson.annotations.SerializedName
import com.nemo.stwatch.db.entity.SmsObj

class SmsResponse {
    @SerializedName("data")
    var data: List<SmsObj>? = null

    @SerializedName("statusCode")
    var statusCode: Int = 0

    @SerializedName("message")
    var message: String = ""


}