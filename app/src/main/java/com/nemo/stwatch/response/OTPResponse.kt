package com.nemo.stwatch.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class OTPResponse{

    @SerializedName("data")
    var data: OTPResponseData? = null

    @SerializedName("statusCode")
    var statusCode: Int = 0

    @SerializedName("message")
    var message: String = ""
}

class OTPResponseData{
    @SerializedName("requestID")
    @Expose
    var requestID: String? = null
}