package com.nemo.stwatch.response

import com.google.gson.annotations.SerializedName
import com.nemo.stwatch.db.entity.SmsObj

class AckResponse{

    @SerializedName("statusCode")
    var statusCode: Int? = 0

}
