package com.nemo.stwatch.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class OTPAuthenResponse {

    @SerializedName("statusCode")
    @Expose
    var statusCode: Int? = null

    @SerializedName("message")
    var message: String = ""
}