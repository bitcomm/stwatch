package com.nemo.stwatch.response

import com.google.gson.annotations.SerializedName
import com.nemo.stwatch.db.entity.SmsObj

class ViewAckResponse{

    @SerializedName("data")
    var data: List<ViewAckItemResponse>? = null
}

class ViewAckItemResponse{

    @SerializedName("timestamp")
    var timestamp: String? = ""

    @SerializedName("mob")
    var mob: String? = ""

    @SerializedName("name")
    var name: String? = ""
}